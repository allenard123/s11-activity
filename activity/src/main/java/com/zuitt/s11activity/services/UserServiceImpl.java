package com.zuitt.s11activity.services;

import com.zuitt.s11activity.models.Post;
import com.zuitt.s11activity.models.User;
import com.zuitt.s11activity.repositories.PostRepository;
import com.zuitt.s11activity.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
//The @Service annotation will allow us to use the CRUD methods inherited from the CRUD repository even though interfaces do not contain implementation/method bodies
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    //Create users
    public void createUser(User user){
        userRepository.save(user);
    }

    //Get users
    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }

    //Deleting a user
    public ResponseEntity deleteUser(Long id){
        userRepository.deleteById(id);
        return new ResponseEntity<>("User deleted successfully!", HttpStatus.OK);
    }

    //Update user
    public ResponseEntity updateUser(Long id, User user){
        User userForUpdating = userRepository.findById(id).get();

        userForUpdating.setUsername(user.getUsername());
        userForUpdating.setPassword(user.getPassword());
        userRepository.save(userForUpdating);

        return new ResponseEntity("User updated successfully!", HttpStatus.OK);
    }

}
