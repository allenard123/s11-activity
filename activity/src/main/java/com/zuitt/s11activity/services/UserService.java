package com.zuitt.s11activity.services;

import com.zuitt.s11activity.models.Post;
import com.zuitt.s11activity.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

    void createUser(User user);

    Iterable<User> getUsers();

    ResponseEntity deleteUser(Long id);

    ResponseEntity updateUser(Long id, User user);

}
